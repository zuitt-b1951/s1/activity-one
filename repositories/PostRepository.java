package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//repository - contains methods database manipulation, which it has inherited from CrudRepository's pre-defined method
//why do we have interface - hide the code's logic (caused abstraction of the codes)
//interface - things used by the user to interact with something like keyboard, remote control, cli ...

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {
}
