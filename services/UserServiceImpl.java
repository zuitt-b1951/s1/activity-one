package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired //injects code from classes/files that we have imported
    private UserRepository userRepository;

    public void createUser(User user){
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Iterable<User> getUser(){
        return userRepository.findAll();
    }

    public ResponseEntity updateUser(Long id, String stringToken, User user){
        User postForUpdating = userRepository.findById(id).get();
        String accountUser = postForUpdating.getUsername();
        String authenticateUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticateUser.equals(accountUser)){
            postForUpdating.setUserName(user.getUsername());
            postForUpdating.setPassword(user.getPassword());
            userRepository.save(postForUpdating);
            return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("You are not authorized to edit the user information", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deleteUser(Long id, String stringToken){
        User postForUpdating = userRepository.findById(id).get();
        String accountUser = postForUpdating.getUsername();
        String authenticateUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticateUser.equals(accountUser)){
            userRepository.deleteById(id);
            return new ResponseEntity<>("User has been successfully deleted", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("You are not authorized to delete this user", HttpStatus.UNAUTHORIZED);
        }
    }
}

