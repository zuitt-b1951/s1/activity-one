package com.zuitt.wdc044.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {


    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @OneToMany(mappedBy = "user")
    @JsonIgnore //to avoid infinite nesting when retrieving post content
    private Set<Post> posts;

    public Set<Post> getPosts(){
        return posts;
    }

    //default constructor
    public User(){}

    //parameterized Constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //getter for the Id
    public Long getId(){
        return id;
    }

    //getter
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }

    //setter
    public void setUserName(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }


}






